#include <jscli.hpp>
#include <jscli/runtime_t.hpp>
#include <jscli/context_t.hpp>
#include <nekjs.hpp>
#include <relapjs.hpp>

#include <quickjs-libc.h>

#include <iostream>
#include <string_view>

static auto new_context(JSRuntime *) -> JSContext *;

auto main(int argc, char ** argv) -> int {
    auto runtime = jscli::runtime_t{};

    js_std_set_worker_new_context_func(new_context);

    js_std_init_handlers(runtime);

    auto context = jscli::context_t{runtime, new_context};

    if (context == nullptr) {
        return 1;
    }

    JS_SetModuleLoaderFunc(runtime, nullptr, js_module_loader, nullptr);

    js_std_add_helpers(context, 0, argv + 1);

    auto module = std::string_view{
        "import * as std from 'std';\n"
        "globalThis.std = std;\n"
        "import * as os from 'os';\n"
        "globalThis.os = os;\n"
        "import * as jscli from 'jscli';\n"
        "globalThis.jscli = jscli;\n"
        "import * as nekjs from 'nekjs';\n"
        "globalThis.nekjs = nekjs;\n"
        "import * as relapjs from 'relapjs';\n"
        "globalThis.relapjs = relapjs;\n"
    };

    auto value = JS_Eval(
        context,
        module.data(),
        module.size(),
        "<input>",
        JS_EVAL_TYPE_MODULE | JS_EVAL_FLAG_COMPILE_ONLY
    );

    if (!JS_IsException(value)) {
        js_module_set_import_meta(context, value, true, true);

        value = JS_EvalFunction(context, value);
    } else {
        js_std_dump_error(context);
    }

    auto script = std::string{};

    for (auto line = std::string{}; std::getline(std::cin, line); ) {
        script += line;
        script += '\n';
    }

    value = JS_Eval(context, script.data(), script.size(), "<jscli>", 0);

    if (JS_IsException(value)) {
        js_std_dump_error(context);
    }

    JS_FreeValue(context, value);

    return 0;
}

auto new_context(JSRuntime * runtime) -> JSContext * {
    auto context = JS_NewContext(runtime);

    if (!context) {
        return nullptr;
    }

    JS_AddIntrinsicBigFloat(context);

    JS_AddIntrinsicBigDecimal(context);

    JS_AddIntrinsicOperators(context);

    JS_EnableBignumExt(context, true);

    js_init_module_std(context, "std");

    js_init_module_os(context, "os");

    jscli::initialize_module(context);

    nekjs::initialize_module(context);

    relapjs::initialize_module(context);

    return context;
}
