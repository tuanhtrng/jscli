relapjs._gfortran_set_args(
    "relap5.js", "-i", "sample.i", "-O", "outdta", "-R", "rstplt"
);

relapjs.blkdta();

relapjs.setreason();

relapjs.timinit();

while (relapjs.ncase() != 0) {
    relapjs.inputd();

    if (relapjs.ncase() == 0) { break; }

    switch(relapjs.problemtype05()) {
        case 1:
        case 2:
        case 3: {
            relapjs.trnctl();
            continue;
        }
        case 4: continue;
        case 5: {
            relapjs.reedit();
            continue;
        }
        case 6: {
            relapjs.strip();
            continue;
        }
        default: relapjs.fabend();
    }
}
