#!/bin/sh

command -v awk >/dev/null 2>&1 || ( apt-get update -y && apt-get install awk -y )
command -v curl >/dev/null 2>&1 || ( apt-get update -y && apt-get install curl -y )
command -v wget >/dev/null 2>&1 || ( apt-get update -y && apt-get install wget -y )
export LATEST_CMAKE=$(curl -Ls -o /dev/null -w %{url_effective} https://github.com/Kitware/CMake/releases/latest | awk -F"/v" '{print $2}')
wget https://github.com/Kitware/CMake/releases/download/v$LATEST_CMAKE/cmake-$LATEST_CMAKE-linux-x86_64.tar.gz
tar -zxvf cmake-$LATEST_CMAKE-linux-x86_64.tar.gz
export PATH=$PATH:$(pwd)/cmake-$LATEST_CMAKE-linux-x86_64/bin
