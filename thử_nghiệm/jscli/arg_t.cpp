#include <catch2/catch_all.hpp>

#include <jscli/arg_t.hpp>

#include <string_view>

SCENARIO("jscli::arg_t có kích thước cố định", "[jscli::arg_t]") {
    static auto arg = jscli::arg_t{};

    arg.resize(7);

    REQUIRE(arg.s.size() == 7);

    REQUIRE(arg.v.size() == 7);

    REQUIRE(arg.s.size() == arg.s.capacity());

    REQUIRE(arg.v.size() == arg.v.capacity());
}

SCENARIO("jscli::arg_t tái dựng argc và argv") {
    auto message = std::string_view{"hello_world"};

    static auto arg = jscli::arg_t{};

    arg.resize(1);

    arg.set(0, message);

    REQUIRE(arg.s.size() == 1);

    REQUIRE(arg.v.size() == 1);

    REQUIRE(arg.v[0] == message);
}

SCENARIO("jscli::arg_t tự quản lý dữ liệu độc lập") {
    auto message = std::string_view{"hello_world"};

    static auto arg0 = jscli::arg_t{};

    arg0.resize(1);

    arg0.set(0, message);

    static auto arg1 = jscli::arg_t{};

    arg1.resize(1);

    arg1.set(0, message);

    REQUIRE(arg0.v.data() != arg1.v.data());

    REQUIRE(arg0.v[0] == message);

    REQUIRE(arg1.v[0] == message);
}
