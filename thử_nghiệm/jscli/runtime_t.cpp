#include <catch2/catch_all.hpp>

#include <jscli/runtime_t.hpp>

SCENARIO("jscli::runtime_t tương ứng với JSRuntime", "[jscli::runtime_t]") {
    GIVEN("khởi tạo đối tượng JSRuntime") {
        auto const runtime = jscli::runtime_t{};

        CHECK(runtime.unique_ptr.get() != nullptr);
    }
}
