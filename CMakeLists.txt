cmake_minimum_required(VERSION 3.20.5)

project(jscli CXX)

add_subdirectory(chương_trình)

add_subdirectory(mã_nguồn)

enable_testing()
add_subdirectory(thử_nghiệm)
