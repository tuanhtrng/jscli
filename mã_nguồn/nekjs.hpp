#ifndef NEKJS_HPP
#define NEKJS_HPP

#include <quickjs.h>

namespace nekjs {

auto initialize_module(JSContext *) -> JSModuleDef *;

}//namespace nekjs

#endif//ndef NEKJS_HPP
