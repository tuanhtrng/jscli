#ifndef NEKJS_FUNC_HPP
#define NEKJS_FUNC_HPP

#include <quickjs.h> 

#include <vector>

namespace nekjs {

extern std::vector<JSCFunctionListEntry> func;

}//namespace nekjs

auto js_nekrs(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

#endif//ndef NEKJS_FUNC_HPP
