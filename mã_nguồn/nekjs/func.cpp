#include "../nekjs.hpp"

#include "../jscli/function_t.hpp"

#include "func.hpp"

std::vector<JSCFunctionListEntry> nekjs::func = {
    jscli::function_t("nekrs", 0, js_nekrs)
};
