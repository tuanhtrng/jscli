#include "../nekjs.hpp"

#include "func.hpp"

static auto initialization(JSContext * context, JSModuleDef * module) -> int;

auto nekjs::initialize_module(JSContext * context) -> JSModuleDef * {
    auto module = JS_NewCModule(context, "nekjs", initialization);

    if (!module) {
        return nullptr;
    }

    JS_AddModuleExportList(context, module, func.data(), func.size());

    return module;
}

auto initialization(JSContext * context, JSModuleDef * module) -> int {
    return JS_SetModuleExportList(
        context, module, nekjs::func.data(), nekjs::func.size()
    );
}
