#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto blkdta(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    blkdta_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
