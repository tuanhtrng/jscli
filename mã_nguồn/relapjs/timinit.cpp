#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto timinit(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    timinit_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
