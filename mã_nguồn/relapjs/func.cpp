#include "../relapjs.hpp"

#include "../jscli/function_t.hpp"

#include "func.hpp"

std::vector<JSCFunctionListEntry> relapjs::func = {
    jscli::function_t("blkdta", 0, blkdta),
    jscli::function_t("fabend", 0, fabend),
    jscli::function_t("_gfortran_set_args", 0, gfortran_set_args),
    jscli::function_t("inputd", 0, inputd),
    jscli::function_t("ncase", 0, ncase),
    jscli::function_t("problemtype05", 0, problemtype05),
    jscli::function_t("reedit", 0, reedit),
    jscli::function_t("setreason", 0, setreason),
    jscli::function_t("strip", 0, strip),
    jscli::function_t("timinit", 0, timinit),
    jscli::function_t("trnctl", 0, trnctl)
};
