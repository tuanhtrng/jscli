#include "../relapjs.hpp"

#include "func.hpp"

static auto init(JSContext * context, JSModuleDef * module) -> int {
    return JS_SetModuleExportList(
        context, module, relapjs::func.data(), relapjs::func.size()
    );
}

auto relapjs::initialize_module(JSContext * context) -> JSModuleDef * {
    auto module = JS_NewCModule(context, "relapjs", init);

    if (!module) {
        return nullptr;
    }

    JS_AddModuleExportList(context, module, func.data(), func.size());

    return module;
}
