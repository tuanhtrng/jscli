#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto trnctl(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    trnctl_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
