#include "../relapjs.hpp"

#include "../jscli/arg_t.hpp"

extern "C" auto _gfortran_set_args(int, char **) -> void;

auto gfortran_set_args(
    JSContext * context, JSValueConst, int argc, JSValueConst * argv
) -> JSValue {
    static auto arg = jscli::arg_t{};

    arg.resize(argc);

    for (auto i = int{0}; i < argc; ++i) {
        arg.set(i, JS_ToCString(context, argv[i]));
    }

    _gfortran_set_args(arg.v.size(), arg.v.data());

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
