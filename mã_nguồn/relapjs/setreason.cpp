#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto setreason(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    setreason_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
