#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto fabend(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    fabend_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
