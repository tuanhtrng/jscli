#ifndef RELAPJS_FUNC_HPP
#define RELAPJS_FUNC_HPP

#include <quickjs.h>

#include <vector>

namespace relapjs {

extern std::vector<JSCFunctionListEntry> func;

}//namespace relapjs

auto blkdta(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto fabend(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto gfortran_set_args(
    JSContext *, JSValueConst, int, JSValueConst *
) -> JSValue;

auto inputd(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto ncase(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto problemtype05(
    JSContext *, JSValueConst, int, JSValueConst *
) -> JSValue;

auto reedit(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto setreason(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto strip(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto timinit(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

auto trnctl(JSContext *, JSValueConst, int, JSValueConst *) -> JSValue;

#endif//ndef RELAPJS_FUNC_HPP
