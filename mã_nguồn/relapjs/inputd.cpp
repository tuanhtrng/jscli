#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto inputd(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    inputd_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
