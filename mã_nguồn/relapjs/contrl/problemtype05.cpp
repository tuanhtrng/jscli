#include "../../relapjs.hpp"

#include <relapxx/contrl.hpp>

auto problemtype05(
    JSContext * context, JSValueConst, int argc, JSValueConst * argv
) -> JSValue {
    if (argc == 1) {
        JS_ToInt32(context, &contrl_.problemtype05, argv[0]);
    }

    return JS_NewInt32(context, contrl_.problemtype05);
}
