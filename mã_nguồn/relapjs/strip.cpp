#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto strip(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    strip_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
