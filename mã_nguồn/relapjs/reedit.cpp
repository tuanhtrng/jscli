#include "../relapjs.hpp"

#include <relapxx/relap_subroutines.hpp>

auto reedit(JSContext * , JSValueConst, int , JSValueConst *) -> JSValue {
    reedit_();

    return JS_MKVAL(JS_TAG_UNDEFINED, 0);
}
