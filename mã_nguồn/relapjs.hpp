#ifndef RELAPJS_HPP
#define RELAPJS_HPP

#include <quickjs.h>

namespace relapjs {

auto initialize_module(JSContext *) -> JSModuleDef *;

}//namespace relapjs

#endif//ndef RELAPJS_HPP
