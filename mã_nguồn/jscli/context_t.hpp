#ifndef JSCLI_CONTEXT_T_HPP
#define JSCLI_CONTEXT_T_HPP

#include <quickjs.h>

#include <memory>

namespace jscli {

template<class Allocator = decltype(JS_NewContext)>
struct context_t {
    using element_type = JSContext;

    using pointer = element_type *;

    using allocator_type = Allocator;

    using deleter_type = decltype(&JS_FreeContext);

    using unique_ptr_t = std::unique_ptr<element_type, deleter_type>;

    context_t(JSRuntime *, allocator_type = JS_NewContext);

    operator pointer() const noexcept;

    unique_ptr_t unique_ptr = {nullptr, JS_FreeContext};
};

}//namespace jscli

template<class Allocator>
jscli::context_t<Allocator>::context_t(
    JSRuntime * runtime, jscli::context_t<Allocator>::allocator_type allocator
) : unique_ptr(allocator(runtime), JS_FreeContext) {
    if (unique_ptr == nullptr) {
        throw;
    }
}

template<class Allocator>
jscli::context_t<Allocator>::operator pointer() const noexcept {
    return unique_ptr.get();
}

#endif//ndef JSCLI_CONTEXT_T_HPP
