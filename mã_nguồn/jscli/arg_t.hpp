#ifndef JSCLI_ARG_T_HPP
#define JSCLI_ARG_T_HPP

#include <cstddef>
#include <string>
#include <vector>

namespace jscli {

struct arg_t {
    std::vector<std::string> s;

    std::vector<char *> v;

    auto set(std::size_t, std::string_view) -> void;

    auto resize(std::size_t) noexcept -> void;
};

}//namespace jscli

#endif//ndef JSCLI_ARG_T_HPP
