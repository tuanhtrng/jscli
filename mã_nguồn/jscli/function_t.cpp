#include "function_t.hpp"

jscli::function_t::function_t(
    std::string_view name, std::uint8_t length, JSCFunction * generic
) : JSCFunctionListEntry({
    .name = name.data(),
    .prop_flags = JS_PROP_WRITABLE | JS_PROP_CONFIGURABLE,
    .def_type = JS_DEF_CFUNC,
    .magic = 0,
    .u = {
        .func = {
            .length = length,
            .cproto = JS_CFUNC_generic,
            .cfunc = {
                .generic = generic
            }
        }
    }
}) {}
