#include "../jscli.hpp"

#include "js/reference.hpp"

static auto initialisation(JSContext * context, JSModuleDef * module) -> int;

auto jscli::initialize_module(JSContext * context) noexcept -> JSModuleDef * {
    auto module = JS_NewCModule(context, "jscli", initialisation);

    if (!module) {
        return nullptr;
    }

    JS_AddModuleExport(context, module, "ReferencePrimitive");

    return module;
}

auto initialisation(JSContext * context, JSModuleDef * module) -> int {
    JS_NewClassID(&jscli::reference->id);

    JS_NewClass(
        JS_GetRuntime(context), jscli::reference->id, &jscli::reference->def
    );

    auto prototype = JS_NewObject(context);

    JS_SetPropertyFunctionList(
        context,
        prototype,
        jscli::reference->proto.data(),
        jscli::reference->proto.size()
    );

    auto reference_class = JS_NewCFunction2(
        context, jscli::reference->ctor, "Reference", 1, JS_CFUNC_constructor, 0
    );

    JS_SetConstructor(context, reference_class, prototype);

    JS_SetClassProto(context, jscli::reference->id, prototype);


    JS_SetModuleExport(context, module, "Reference", reference_class);

    return 0;
}
