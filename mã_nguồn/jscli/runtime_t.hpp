#ifndef JSCLI_RUNTIME_T_HPP
#define JSCLI_RUNTIME_T_HPP

#include <quickjs.h>

#include <memory>
#include <utility>

namespace jscli {

template<class Allocator = decltype(JS_NewRuntime)>
struct runtime_t {
    using element_type = JSRuntime;

    using pointer = element_type *;

    using allocator_type = Allocator;

    using deleter_type = decltype(&JS_FreeRuntime);

    using unique_ptr_t = std::unique_ptr<element_type, deleter_type>;

    template<class... Ts>
    runtime_t(allocator_type = JS_NewRuntime, Ts...);

    operator pointer() const noexcept;

    unique_ptr_t unique_ptr;
};

}//namespace jscli

template<class Allocator>
template<class... Ts>
jscli::runtime_t<Allocator>::runtime_t(
    jscli::runtime_t<Allocator>::allocator_type allocator, Ts... args
) : unique_ptr(allocator(std::forward<Ts>(args)...), JS_FreeRuntime) {
    if (unique_ptr == nullptr) {
        throw;
    }
}

template<class Allocator>
jscli::runtime_t<Allocator>::operator pointer() const noexcept {
    return unique_ptr.get();
}

#endif//ndef JSCLI_RUNTIME_T_HPP
