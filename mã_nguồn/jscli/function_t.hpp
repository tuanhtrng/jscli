#ifndef JSCLI_FUNCTION_T_HPP
#define JSCLI_FUNCTION_T_HPP

#include <quickjs.h>

#include <cstdint>
#include <string_view>

namespace jscli {

struct function_t : JSCFunctionListEntry {
    function_t(std::string_view, std::uint8_t, JSCFunction *);
};

}//namespace jscli

#endif//ndef JSCLI_FUNCTION_T_HPP
