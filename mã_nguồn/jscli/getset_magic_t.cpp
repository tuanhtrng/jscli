#include "getset_magic_t.hpp"

jscli::getset_magic_t::getset_magic_t(
    std::string_view name,
    jscli::getset_magic_t::getter_magic_t fgetter,
    jscli::getset_magic_t::setter_magic_t fsetter,
    std::int16_t magic
) : JSCFunctionListEntry({
    .name = name.data(),
    .prop_flags = JS_PROP_CONFIGURABLE,
    .def_type = JS_DEF_CGETSET_MAGIC,
    .magic = magic,
    .u = {
        .getset = {
            .get = {
                .getter_magic = fgetter 
            },
            .set = {
                .setter_magic = fsetter 
            }
        }
    }
}) {}
