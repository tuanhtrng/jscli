#include "arg_t.hpp"

#include <iterator>

auto jscli::arg_t::resize(std::size_t size) noexcept -> void {
    s.resize(size);

    v.resize(size);
}

auto jscli::arg_t::set(std::size_t pos, std::string_view sv) -> void {
    s[pos] = std::string{sv};

    v[pos] = s[pos].data();
}
