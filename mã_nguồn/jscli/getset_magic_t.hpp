#ifndef JSCLI_GETSET_MAGIC_T_HPP
#define JSCLI_GETSET_MAGIC_T_HPP

#include <quickjs.h>

#include <cstdint>
#include <string_view>

namespace jscli {

struct getset_magic_t : JSCFunctionListEntry {
    using getter_magic_t = decltype(JSCFunctionType::getter_magic);

    using setter_magic_t = decltype(JSCFunctionType::setter_magic);

    getset_magic_t(
        std::string_view, getter_magic_t, setter_magic_t, std::int16_t
    );
};

}//namespace jscli

#endif//ndef JSCLI_GETSET_MAGIC_T_HPP
