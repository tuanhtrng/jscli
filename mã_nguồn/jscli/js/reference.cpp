#include "reference.hpp"

#include "../getset_magic_t.hpp"

#include <cstdint>

struct reference_t {
    union {
        std::int64_t int64;
        std::int32_t int32; 
        bool boolean;
        double float64;
    } u;
};

static auto constructor(
    JSContext *, JSValueConst, int, JSValueConst *
) -> JSValue;

static auto finalizer(JSRuntime *, JSValue) -> void;

static auto getter(JSContext *, JSValueConst, int) -> JSValue;

static auto setter(JSContext *, JSValueConst, JSValue, int) -> JSValue;

static auto reference_backend = jscli::class_t{
    .ctor = &constructor,
    .def = {
        .class_name = "Reference",
        .finalizer = &finalizer
    },
    .proto = {
        jscli::getset_magic_t("int64", getter, setter, JS_TAG_BIG_INT),
        jscli::getset_magic_t("int32", getter, setter, JS_TAG_INT),
        jscli::getset_magic_t("boolean", getter, setter, JS_TAG_BOOL),
        jscli::getset_magic_t("float64", getter, setter, JS_TAG_FLOAT64)
    }
};

jscli::class_t * jscli::reference = &reference_backend;

auto constructor(
    JSContext * context, JSValueConst new_target, int argc, JSValueConst * argv
) -> JSValue {
    auto data = static_cast<reference_t *>(
        js_mallocz(context, sizeof(reference_t))
    );

    if (!data) {
        return JS_EXCEPTION;
    }

    auto const value = argv[0];

    switch(JS_VALUE_GET_TAG(value)) {
        case JS_TAG_BIG_INT: {
            auto int64 = std::int64_t{};

            if (JS_ToInt64(context, &int64, value)) {
                return JS_EXCEPTION;
            }

            data->u.int64 = int64;

            break;
        }
        case JS_TAG_INT: {
            auto int32 = std::int32_t{};

            if (JS_ToInt32(context, &int32, value)) {
                return JS_EXCEPTION;
            }

            data->u.int32 = int32;

            break;
        }
        case JS_TAG_BOOL: {
            auto const boolean = JS_ToBool(context, value);
            
            if (boolean == -1) {
                return JS_EXCEPTION;
            }

            data->u.boolean = boolean;

            break;
        }
        case JS_TAG_FLOAT64: {
            auto float64 = double{};

            if (JS_ToFloat64(context, &float64, value)) {
                return JS_EXCEPTION;
            }

            data->u.float64 = float64;

            break;
        }
    }

    auto prototype = JS_GetPropertyStr(context, new_target, "prototype");

    if (JS_IsException(prototype)) {
        js_free(context, data);

        return JS_EXCEPTION;
    }

    auto object = JS_NewObjectProtoClass(
        context, prototype, jscli::reference->id
    );

    JS_FreeValue(context, prototype);

    if (JS_IsException(object)) {
        js_free(context, data);

        JS_FreeValue(context, object);

        return JS_EXCEPTION;
    }

    JS_SetOpaque(object, data);

    return object;
};

auto finalizer(JSRuntime * runtime, JSValue value) -> void {
    auto * object = static_cast<reference_t *>(
        JS_GetOpaque(value, jscli::reference->id)
    );

    js_free_rt(runtime, object);
}

auto getter(JSContext * context, JSValueConst this_value, int magic) -> JSValue {
    auto data = static_cast<reference_t *>(
        JS_GetOpaque2(context, this_value, jscli::reference->id)
    );

    if (!data) {
        return JS_EXCEPTION;
    }

    switch(magic) {
        case JS_TAG_BIG_INT:
            return JS_NewInt64(context, data->u.int64);
        case JS_TAG_INT:
            return JS_NewInt32(context, data->u.int32);
        case JS_TAG_BOOL:
            return JS_NewBool(context, data->u.boolean);
        case JS_TAG_FLOAT64:
            return JS_NewFloat64(context, data->u.float64);
        default:
            return JS_UNINITIALIZED;
    }
}

auto setter(
    JSContext * context, JSValueConst this_value, JSValue value, int magic
) -> JSValue {
    auto data = static_cast<reference_t *>(
        JS_GetOpaque2(context, this_value, jscli::reference->id)
    );

    if (!data) {
        return JS_EXCEPTION;
    }

    switch(magic) {
        case JS_TAG_BIG_INT: {
            auto int64 = std::int64_t{};

            if (JS_ToInt64(context, &int64, value)) {
                return JS_EXCEPTION;
            }

            data->u.int64 = int64;

            break;
        }
        case JS_TAG_INT: {
            auto int32 = std::int32_t{};

            if (JS_ToInt32(context, &int32, value)) {
                return JS_EXCEPTION;
            }

            data->u.int32 = int32;

            break;
        }
        case JS_TAG_BOOL: {
            auto const boolean = JS_ToBool(context, value);
            
            if (boolean == -1) {
                return JS_EXCEPTION;
            }

            data->u.boolean = boolean;

            break;
        }
        case JS_TAG_FLOAT64: {
            auto float64 = double{};

            if (JS_ToFloat64(context, &float64, value)) {
                return JS_EXCEPTION;
            }

            data->u.float64 = float64;

            break;
        }
    }

    return JS_UNDEFINED;
}
