#ifndef JSCLI_REFERENCE_HPP
#define JSCLI_REFERENCE_HPP

#include "class_t.hpp"

namespace jscli {

extern class_t * reference;

}//namespace jscli

#endif//ndef JSCLI_REFERENCE_HPP
