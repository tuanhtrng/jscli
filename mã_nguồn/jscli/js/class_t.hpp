#ifndef JSCLI_CLASS_T_HPP
#define JSCLI_CLASS_T_HPP

#include <quickjs.h>

#include <vector>

namespace jscli {

struct class_t {
    JSClassID id;
    JSCFunction * ctor;
    JSClassDef def;
    std::vector<JSCFunctionListEntry> proto;
};

}//namespace jscli

#endif//ndef JSCLI_CLASS_T_HPP
