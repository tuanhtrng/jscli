#ifndef JSCLI_HPP
#define JSCLI_HPP

#include <quickjs.h>

namespace jscli {

auto initialize_module(JSContext *) noexcept -> JSModuleDef *;

}//namespace jscli

#endif//ndef JSCLI_HPP
